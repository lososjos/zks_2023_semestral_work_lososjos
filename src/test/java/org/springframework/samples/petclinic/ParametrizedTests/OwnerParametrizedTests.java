package org.springframework.samples.petclinic.ParametrizedTests;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.OwnerController;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.validation.BindingResult;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class OwnerParametrizedTests {

	private Validator createValidator() {
		LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
		localValidatorFactoryBean.afterPropertiesSet();
		return localValidatorFactoryBean;
	}

	private String parseInput(String input) {
		if (input.equals("null"))
			return null;
		if (input.equals("empty"))
			return "";
		return input;
	}

	@ParameterizedTest
	@CsvFileSource(files = "src/test/resources/owner-input-data.csv", numLinesToSkip = 1)
	void test_create_new_owner(String firstname, String lastname, String address, String city, String telephone,
			String expectedResult, Integer expectedErrorCount) {
		BindingResult result = new MapBindingResult(new HashMap<>(), "binding");
		OwnerRepository ownerRepository = mock(OwnerRepository.class);
		OwnerController ownerController = new OwnerController(ownerRepository);
		Owner owner = new Owner();
		owner.setId(1); // it is used in expected result
		owner.setFirstName(parseInput(firstname));
		owner.setLastName(parseInput(lastname));
		owner.setAddress(parseInput(address));
		owner.setCity(parseInput(city));
		owner.setTelephone(parseInput(telephone));

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that there is correct count of violations
		assertEquals(expectedErrorCount, constraintViolations.size());
		if (!constraintViolations.isEmpty()) {
			ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
			result.rejectValue(violation.getPropertyPath().toString(), violation.getMessage());
		}

		String resultString = ownerController.processCreationForm(owner, result);

		// Assert that returned URL is required one
		assertEquals(expectedResult, resultString);
	}

}
