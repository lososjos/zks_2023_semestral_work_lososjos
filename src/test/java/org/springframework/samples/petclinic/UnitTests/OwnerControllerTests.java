package org.springframework.samples.petclinic.UnitTests;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.OwnerController;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class OwnerControllerTests {

	private static final String VIEWS_OWNER_CREATE_OR_UPDATE_FORM = "owners/createOrUpdateOwnerForm";

	private BindingResult result;

	private ModelMap model;

	private OwnerRepository ownerRepository;

	private OwnerController ownerController;

	private Validator createValidator() {
		LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
		localValidatorFactoryBean.afterPropertiesSet();
		return localValidatorFactoryBean;
	}

	@BeforeEach
	public void setUp() {
		model = new ModelMap();
		result = new MapBindingResult(new HashMap<>(), "binding");
		ownerRepository = mock(OwnerRepository.class);
		ownerController = new OwnerController(ownerRepository);
	}

	@Test
	@Tag("success")
	public void test_findOwner_with_given_ownerId_found_successfully() {
		Integer ownerId = 1;
		Owner owner = new Owner();
		owner.setId(ownerId);

		// When findById is called, return owner with required id
		Mockito.when(ownerRepository.findById(owner.getId())).thenReturn(owner);

		Owner result = ownerController.findOwner(ownerId);

		// Assert that result is valid owner with required id
		assertNotNull(result);
		assertEquals(ownerId, result.getId());
	}

	@Test
	@Tag("fail")
	public void test_findOwner_ownerId_not_found_should_return_null() {
		Integer ownerId = 1;

		// When findById is called, return null
		Mockito.when(ownerRepository.findById(Mockito.anyInt())).thenReturn(null);

		Owner result = ownerController.findOwner(ownerId);

		// Assert that null was returned
		assertNull(result);
	}

	@Test
	@Tag("fail")
	public void test_findOwner_returns_empty_owner_if_ownerId_is_null() {
		Integer ownerId = null;

		Owner result = ownerController.findOwner(ownerId);

		// Assert that new Owner is returned
		assertNotNull(result);
		assertNull(result.getId());
	}

	@Test
	@Tag("success")
	public void test_initCreationForm_with_valid_owner_object() {

		String resultString = ownerController.initCreationForm(model);

		// Assert
		// Result should be the createOrUpdateOwnerForm view and new owner in model
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
		assertNotNull(model.get("owner"));
	}

	@Test
	@Tag("success")
	public void test_processCreationForm_new_owner_successfully() {
		Owner owner = new Owner();
		owner.setId(1);
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("my Address");
		owner.setCity("New York");
		owner.setTelephone("1234567890");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that there are no errors in owner object
		assertTrue(constraintViolations.isEmpty());

		String resultString = ownerController.processCreationForm(owner, result);

		// Assert that correct redirect URL is returned
		assertEquals("redirect:/owners/" + owner.getId(), resultString);
		// Verify that save owner into repository is called
		Mockito.verify(ownerRepository).save(owner);
	}

	@Test
	@Tag("fail")
	public void test_processCreationForm_new_owner_non_numeric_telephone() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("my Address");
		owner.setCity("New York");
		owner.setTelephone("123abc");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that telephone must be numeric value
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("telephone", violation.getPropertyPath().toString());
		Assertions.assertEquals("numeric value out of bounds (<10 digits>.<0 digits> expected)",
				violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "Wrong format", violation.getMessage());

		String resultString = ownerController.processCreationForm(owner, result);
		// Result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processCreationForm_new_owner_empty_first_name() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		Owner owner = new Owner();
		owner.setFirstName("");
		owner.setLastName("Wick");
		owner.setAddress("my Address");
		owner.setCity("New York");
		owner.setTelephone("1234567890");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that firstName must not be blank
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("firstName", violation.getPropertyPath().toString());
		Assertions.assertEquals("must not be blank", violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "emptyInput", violation.getMessage());

		String resultString = ownerController.processCreationForm(owner, result);
		// Result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processCreationForm_new_owner_empty_last_name() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("");
		owner.setAddress("my Address");
		owner.setCity("New York");
		owner.setTelephone("1234567890");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that lastName must not be blank
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("lastName", violation.getPropertyPath().toString());
		Assertions.assertEquals("must not be blank", violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "emptyInput", violation.getMessage());

		String resultString = ownerController.processCreationForm(owner, result);
		// Result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processCreationForm_new_owner_empty_address() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("");
		owner.setCity("New York");
		owner.setTelephone("1234567890");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that address must not be blank
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("address", violation.getPropertyPath().toString());
		Assertions.assertEquals("must not be blank", violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "emptyInput", violation.getMessage());

		String resultString = ownerController.processCreationForm(owner, result);
		// Result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processCreationForm_new_owner_empty_city() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("my address");
		owner.setCity("");
		owner.setTelephone("1234567890");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that city must not be blank
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("city", violation.getPropertyPath().toString());
		Assertions.assertEquals("must not be blank", violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "emptyInput", violation.getMessage());

		String resultString = ownerController.processCreationForm(owner, result);
		// Result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processCreationForm_new_owner_empty_telephone() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("my Address");
		owner.setCity("New York");
		owner.setTelephone("");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);
		Iterator<ConstraintViolation<Owner>> i = constraintViolations.iterator();

		// Assert that telephone must not be blank and must be numeric value
		Assertions.assertEquals(2, constraintViolations.size());
		ConstraintViolation<Owner> violation = i.next();

		result.rejectValue(violation.getPropertyPath().toString(), "emptyInput", violation.getMessage());

		String resultString = ownerController.processCreationForm(owner, result);
		// Result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processCreationForm_new_owner_too_long_telephone() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("my Address");
		owner.setCity("New York");
		owner.setTelephone("11234567890");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that telephone must be numeric value of max 10 digits
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("telephone", violation.getPropertyPath().toString());
		Assertions.assertEquals("numeric value out of bounds (<10 digits>.<0 digits> expected)",
				violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "tooLong", violation.getMessage());

		String resultString = ownerController.processCreationForm(owner, result);
		// Result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("success")
	public void test_processFindForm_owner_lastname_is_null_should_set_owners_lastname_to_empty_string() {
		Owner owner = new Owner();
		owner.setLastName(null);
		Model model = new ExtendedModelMap();
		Page<Owner> ownersResults = new PageImpl<>(Collections.emptyList());

		// When findByLastName is called return empty owners page
		Mockito.when(ownerRepository.findByLastName(Mockito.anyString(), Mockito.any())).thenReturn(ownersResults);

		String resultString = ownerController.processFindForm(1, owner, result, model);

		// Assert that lastname is not null
		assertNotNull(owner.getLastName());
		assertEquals("owners/findOwners", resultString);
	}

	@Test
	@Tag("success")
	public void test_processFindForm_no_owners_found() {
		Owner owner = new Owner();
		owner.setLastName("Wick");
		Page<Owner> ownersResults = new PageImpl<>(Collections.emptyList());
		Model model = new ExtendedModelMap();

		// When findByLastName is called return empty list of owners
		Mockito.when(ownerRepository.findByLastName(Mockito.anyString(), Mockito.any())).thenReturn(ownersResults);

		String resultString = ownerController.processFindForm(1, owner, result, model);

		// Assert returned URL and error of lastname exists
		assertEquals("owners/findOwners", resultString);
		assertTrue(result.getFieldErrors().stream().anyMatch(e -> e.getField().equals("lastName")));
	}

	@Test
	@Tag("success")
	public void test_processFindForm_one_owner_found() {
		Owner owner = new Owner();
		owner.setId(1);
		owner.setLastName("Smith");
		Page<Owner> ownersResults = new PageImpl<>(Collections.singletonList(owner));

		// When findByLastName is called page of owners with one element is returned
		Mockito.when(ownerRepository.findByLastName(Mockito.anyString(), Mockito.any(Pageable.class)))
			.thenReturn(ownersResults);
		Model model = new ExtendedModelMap();

		String resultString = ownerController.processFindForm(1, owner, result, model);

		// Assert that URL to redirect to owners page is returned
		assertEquals("redirect:/owners/" + owner.getId(), resultString);
	}

	@Test
	@Tag("success")
	public void test_processFindForm_multiple_owners_found() {
		Owner ownerOne = new Owner();
		ownerOne.setFirstName("John");
		ownerOne.setLastName("Wick");
		Owner ownerTwo = new Owner();
		ownerTwo.setFirstName("Jason");
		ownerTwo.setLastName("Wick");
		Model model = new ExtendedModelMap();
		Page<Owner> ownersResults = new PageImpl<>(Arrays.asList(ownerOne, ownerTwo));

		// When findByName is called owners Page with more than one element in it is
		// returned
		Mockito.when(ownerRepository.findByLastName(Mockito.anyString(), Mockito.any(Pageable.class)))
			.thenReturn(ownersResults);

		String resultString = ownerController.processFindForm(1, ownerOne, result, model);

		// Assert that URL to page with multiple results is returned
		assertEquals("owners/ownersList", resultString);
	}

	@Test
	@Tag("success")
	public void test_processFindForm_multiple_owners_found_on_more_pages() {
		int requiredPageNumber = 2;
		long ownersCount = 11;

		Model model = new ExtendedModelMap();
		List<Owner> owners = new ArrayList<>(Collections.emptyList());
		// Generate ownersCount of owners
		for (int i = 0; i < ownersCount; i++) {
			Owner owner = new Owner();
			owner.setFirstName("John" + i);
			owner.setLastName("Wick");
			owners.add(owner);
		}
		Page<Owner> ownersResults = new PageImpl<>(owners);

		// When findByLast is called return Page with 'ownersCount' elements
		Mockito.when(ownerRepository.findByLastName(Mockito.anyString(), Mockito.any(Pageable.class)))
			.thenReturn(ownersResults);

		String resultString = ownerController.processFindForm(requiredPageNumber, owners.get(0), result, model);

		// Assert that URL to list of owners is returned
		assertEquals("owners/ownersList", resultString);
		// Verify that correct page is returned
		assertEquals(requiredPageNumber, model.getAttribute("currentPage"));
		// Verify that correct number of pages is returned
		assertEquals(1, model.getAttribute("totalPages"));
		// Verify that whole result has right number of items
		assertEquals(ownersCount, model.getAttribute("totalItems"));
		// Verify that return list of owners is correct
		assertEquals(owners, model.getAttribute("listOwners"));
	}

	@Test
	@Tag("success")
	public void test_initUpdateOwnerForm_with_valid_ownerId() {
		int ownerId = 1;
		Owner owner = new Owner();
		owner.setId(ownerId);
		Model model = mock(Model.class);

		// When findById is called owner is returned
		Mockito.when(ownerRepository.findById(ownerId)).thenReturn(owner);

		String resultString = ownerController.initUpdateOwnerForm(ownerId, model);

		// Assert that result should be the createOrUpdateOwnerForm view and
		// should be called adding of owner into model
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
		Mockito.verify(model).addAttribute(owner);
	}

	@Test
	@Tag("success")
	public void test_processUpdateOwnerForm_update_owner_successfully() {
		int ownerId = 2;
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("my new Address");
		owner.setCity("New York");
		owner.setTelephone("1234567890");

		String resultString = ownerController.processUpdateOwnerForm(owner, result, ownerId);

		// Assert that correct redirect URL is returned
		assertEquals("redirect:/owners/{ownerId}", resultString);
		// Verify that owner id is set to ownerId
		assertEquals(ownerId, owner.getId());
		// Verify that save owner into repository is called
		Mockito.verify(ownerRepository).save(owner);
	}

	@Test
	@Tag("fail")
	public void test_processUpdateOwnerForm_update_owner_telephone_to_non_numeric_should_fail() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		int ownerId = 2;
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("my Address");
		owner.setCity("New York");
		owner.setTelephone("phone123");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that telephone must be numeric value
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("telephone", violation.getPropertyPath().toString());
		Assertions.assertEquals("numeric value out of bounds (<10 digits>.<0 digits> expected)",
				violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "Wrong format", violation.getMessage());

		String resultString = ownerController.processUpdateOwnerForm(owner, result, ownerId);

		// Assert that result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processUpdateOwnerForm_update_owner_to_empty_first_name_should_fail() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		int ownerId = 2;
		Owner owner = new Owner();
		owner.setFirstName("");
		owner.setLastName("Wick");
		owner.setAddress("my Address");
		owner.setCity("New York");
		owner.setTelephone("1234567890");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that firstName must not be blank
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("firstName", violation.getPropertyPath().toString());
		Assertions.assertEquals("must not be blank", violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "emptyInput", violation.getMessage());

		String resultString = ownerController.processUpdateOwnerForm(owner, result, ownerId);
		// Assert that result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processUpdateOwnerForm_update_owner_to_empty_last_name_should_fail() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		int ownerId = 2;
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("");
		owner.setAddress("my Address");
		owner.setCity("New York");
		owner.setTelephone("1234567890");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that lastName must not be blank
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("lastName", violation.getPropertyPath().toString());
		Assertions.assertEquals("must not be blank", violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "emptyInput", violation.getMessage());

		String resultString = ownerController.processUpdateOwnerForm(owner, result, ownerId);
		// Assert that result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processUpdateOwnerForm_update_owner_to_empty_address_should_fail() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		int ownerId = 2;
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("");
		owner.setCity("New York");
		owner.setTelephone("1234567890");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that address must not be blank
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("address", violation.getPropertyPath().toString());
		Assertions.assertEquals("must not be blank", violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "emptyInput", violation.getMessage());

		String resultString = ownerController.processUpdateOwnerForm(owner, result, ownerId);
		// Assert that result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processUpdateOwnerForm_update_owner_to_empty_city() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		int ownerId = 2;
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("my address");
		owner.setCity("");
		owner.setTelephone("1234567890");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that city must not be blank
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("city", violation.getPropertyPath().toString());
		Assertions.assertEquals("must not be blank", violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "emptyInput", violation.getMessage());

		String resultString = ownerController.processUpdateOwnerForm(owner, result, ownerId);
		// Assert that result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processUpdateOwnerForm_update_owner_to_empty_telephone() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		int ownerId = 2;
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("my Address");
		owner.setCity("New York");
		owner.setTelephone("");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);
		Iterator<ConstraintViolation<Owner>> i = constraintViolations.iterator();

		// Assert that telephone must not be blank and must be numeric value
		Assertions.assertEquals(2, constraintViolations.size());
		ConstraintViolation<Owner> violation = i.next();
		ConstraintViolation<Owner> violation2 = i.next();

		result.rejectValue(violation.getPropertyPath().toString(), "emptyInput", violation.getMessage());
		result.rejectValue(violation2.getPropertyPath().toString(), "wrongFormat", violation2.getMessage());

		String resultString = ownerController.processUpdateOwnerForm(owner, result, ownerId);
		// Assert that result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("fail")
	public void test_processUpdateOwnerForm_update_owner_to_too_long_telephone() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		int ownerId = 2;
		Owner owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("my Address");
		owner.setCity("New York");
		owner.setTelephone("11234567890");

		Validator validator = createValidator();
		Set<ConstraintViolation<Owner>> constraintViolations = validator.validate(owner);

		// Assert that telephone must be numeric value of max 10 digits
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Owner> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("telephone", violation.getPropertyPath().toString());
		Assertions.assertEquals("numeric value out of bounds (<10 digits>.<0 digits> expected)",
				violation.getMessage());

		result.rejectValue(violation.getPropertyPath().toString(), "tooLong", violation.getMessage());

		String resultString = ownerController.processUpdateOwnerForm(owner, result, ownerId);
		// Assert that result should be the createOrUpdateOwnerForm view
		assertEquals(VIEWS_OWNER_CREATE_OR_UPDATE_FORM, resultString);
	}

	@Test
	@Tag("success")
	public void test_showOwner_valid_ownerId_returns_model_and_view_with_correct_view_name() {
		ModelAndView expectedModelAndView = new ModelAndView("owners/ownerDetails");
		int ownerId = 1;
		Owner owner = new Owner();
		owner.setId(ownerId);

		// When findById is called owner is returned
		Mockito.when(ownerRepository.findById(ownerId)).thenReturn(owner);

		ModelAndView actualModelAndView = ownerController.showOwner(ownerId);

		// Assert that model and view name is return correctly
		assertEquals(expectedModelAndView.getViewName(), actualModelAndView.getViewName());
	}

}
