package org.springframework.samples.petclinic.UnitTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.samples.petclinic.owner.*;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.MapBindingResult;

import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class PetControllerTests {

	private static final String VIEWS_PETS_CREATE_OR_UPDATE_FORM = "pets/createOrUpdatePetForm";

	private static final String processCreationOrUpdateFormOk = "redirect:/owners/{ownerId}";

	private Owner owner;

	private Owner mockedOwner;

	private BindingResult result;

	private ModelMap model;

	private OwnerRepository ownerRepository;

	private PetController petController;

	private Map<String, PetType> petTypes;

	@BeforeEach
	public void setUp() {
		// Create a testing Owner
		owner = new Owner();
		owner.setFirstName("John");
		owner.setLastName("Wick");
		owner.setAddress("home");
		owner.setCity("myCity");
		owner.setTelephone("987654321");
		mockedOwner = Mockito.mock(Owner.class);

		model = new ModelMap();
		result = new MapBindingResult(new HashMap<>(), "binding");

		ownerRepository = mock(OwnerRepository.class);
		petController = new PetController(ownerRepository);
		// testing petTypes
		List<String> types = Arrays.asList("dog", "bird", "cat", "hamster", "lizard", "snake");
		petTypes = new HashMap<>();
		for (String type : types) {
			PetType petType = new PetType();
			petType.setName(type);
			petTypes.put(type, petType);
		}
	}

	@Test
	public void test_findOwner_should_return_owner_with_given_id_successfully() {
		int ownerId = 1;
		Owner expectedOwner = new Owner();
		expectedOwner.setId(ownerId);

		// When the findById method is called, return valid owner with given id
		Mockito.when(ownerRepository.findById(ownerId)).thenReturn(expectedOwner);

		Owner actualOwner = petController.findOwner(ownerId);

		// Assert
		assertEquals(expectedOwner, actualOwner);
	}

	@Test
	public void test_findOwner_should_throw_exception_owner_not_found() {
		int ownerId = 1;

		// When the findById method is called, return null = owner not found
		Mockito.when(ownerRepository.findById(Mockito.anyInt())).thenReturn(null);

		// Assert that exception is thrown
		Exception e = assertThrows(IllegalArgumentException.class, () -> {
			petController.findOwner(ownerId);
		});
		// Assert that error message is correct
		assertEquals("Owner ID not found: " + ownerId, e.getMessage());

	}

	@Test
	public void test_findPet_returns_existing_pet_successfully() {
		mockedOwner.setId(1);
		Pet pet = new Pet();
		pet.setId(1);

		// When the findById method is called, return valid owner
		Mockito.when(ownerRepository.findById(1)).thenReturn(mockedOwner);
		// When the owner's getPet method is called, return valid pet
		Mockito.when(mockedOwner.getPet(pet.getId())).thenReturn(pet);

		Pet result = petController.findPet(1, 1);

		// Assert
		assertEquals(pet, result);
	}

	@Test
	public void test_findPet_returns_new_pet_object() {
		owner.setId(1);

		// When the findById method is called, return valid owner
		Mockito.when(ownerRepository.findById(1)).thenReturn(owner);

		Pet result = petController.findPet(1, null);

		assertNotNull(result);
		assertTrue(result.isNew());
	}

	@Test
	public void test_initCreationForm_with_valid_owner_object() {

		String resultString = petController.initCreationForm(owner, model);

		// Assert
		// Result should be the createOrUpdatePetForm view and new pet in model
		assertEquals(VIEWS_PETS_CREATE_OR_UPDATE_FORM, resultString);
		assertNotNull(model.get("pet"));
		assertTrue(((Pet) model.get("pet")).isNew());
	}

	@Test
	public void test_processCreationForm_new_pet_with_today_birthdate_successfully() {
		Pet pet = new Pet();
		pet.setName("Max");
		pet.setBirthDate(LocalDate.now());

		String resultString = petController.processCreationForm(owner, pet, result, model);

		// Assert
		// Result should be the OK redirect URL
		assertEquals(processCreationOrUpdateFormOk, resultString);
		// Verify that there are no errors
		assertFalse(result.hasErrors());
		// Verify that pet is added to the owner's list of pets
		assertTrue(owner.getPets().contains(pet));
	}

	@Test
	public void test_processCreationForm_new_pet_with_future_birthdate_should_return_error() {
		Pet pet = new Pet();
		pet.setName("Max");
		pet.setBirthDate(LocalDate.now().plusDays(1)); // Set birthdate in the future

		String resultString = petController.processCreationForm(owner, pet, result, model);

		// Assert
		// Should return the createOrUpdatePetForm view
		assertEquals(VIEWS_PETS_CREATE_OR_UPDATE_FORM, resultString);
		// Should return error of birthdate
		assertTrue(result.hasErrors());
		assertTrue(result.getFieldErrors().stream().anyMatch(e -> e.getField().equals("birthDate")));
	}

	@Test
	public void test_processCreationForm_new_pet_with_existing_name_should_return_error() {
		Pet pet = new Pet();
		pet.setName("Max");
		pet.setBirthDate(LocalDate.of(2020, 1, 1));
		pet.setType(petTypes.get("dog"));

		// Add fist pet of the given name
		String resultFirstPet = petController.processCreationForm(mockedOwner, pet, result, model);

		// When the owner's getPet method is called, return pet
		Mockito.when(mockedOwner.getPet(Mockito.anyString(), Mockito.anyBoolean())).thenReturn(pet);

		Pet newPet = new Pet();
		newPet.setName("Max"); // Existing pet name
		newPet.setBirthDate(LocalDate.of(2021, 1, 1));
		newPet.setType(petTypes.get("cat"));

		// Add pet with already existing name
		String resultSecondPet = petController.processCreationForm(mockedOwner, newPet, result, model);

		// Assert
		// Should process OK for the first pet and
		assertEquals(processCreationOrUpdateFormOk, resultFirstPet);
		// return the createOrUpdatePetForm view with error of duplicated name for the
		// second pet
		assertEquals(VIEWS_PETS_CREATE_OR_UPDATE_FORM, resultSecondPet);
		assertTrue(result.hasErrors());
		assertNotNull(result.getFieldError("name"));
		assertEquals("duplicate", result.getFieldError("name").getCode());
	}

	@Test
	public void test_processCreationForm_new_valid_pet_add_to_owner_list_successfully() {
		Pet pet = new Pet();
		pet.setName("Max");
		pet.setBirthDate(LocalDate.of(2020, 1, 1));
		pet.setType(petTypes.get("snake"));

		String resultString = petController.processCreationForm(owner, pet, result, model);

		// Assert
		// Pet should be added to the owner's list of pets and result is OK redirect URL
		assertEquals(processCreationOrUpdateFormOk, resultString);
		assertTrue(owner.getPets().contains(pet));
	}

	@Test
	public void test_initUpdateForm_with_valid_pet_object() {
		Pet pet = new Pet();
		pet.setName("python");
		pet.setType(petTypes.get("snake"));
		pet.setBirthDate(LocalDate.now().minusDays(1)); // Set birthdate in the past

		mockedOwner.addPet(pet);
		// When the owner's getPet method is called, return pet
		Mockito.when(mockedOwner.getPet(Mockito.anyInt())).thenReturn(pet);

		String resultString = petController.initUpdateForm(mockedOwner, 1, model);

		// Assert
		// Result should be the createOrUpdatePetForm view pet in model
		assertEquals(VIEWS_PETS_CREATE_OR_UPDATE_FORM, resultString);
		assertEquals(pet, model.get("pet"));
	}

	@Test
	public void test_processUpdateForm_set_pet_new_name_successfully() {
		Pet pet = new Pet();
		pet.setName("Drak mrak");

		// When the owner's getPet method is called, return null = pet with new name not
		// exist yet
		Mockito.when(mockedOwner.getPet(pet.getName().toLowerCase(), false)).thenReturn(null);

		String resultString = petController.processUpdateForm(pet, result, mockedOwner, model);

		// Assert
		// Result should be the expected redirect URL
		assertEquals(processCreationOrUpdateFormOk, resultString);
		// Verify that the owner's addPet method was called with the pet
		Mockito.verify(mockedOwner).addPet(pet);
		// Verify that the owner's save method was called
		Mockito.verify(ownerRepository).save(mockedOwner);
	}

	@Test
	public void test_processUpdateForm_set_pet_new_name_return_duplicate_error() {
		// pet which owner already have in his list of pets
		Pet pet = new Pet();
		pet.setId(1);
		pet.setName("Drak mrak");
		pet.setType(petTypes.get("lizard"));
		// new pet to which wants to add
		Pet newPet = new Pet();
		newPet.setName("Drak mrak");
		newPet.setType(petTypes.get("lizard"));

		// When the owner's getPet method is called, return pet = pet with this name
		// already exist
		Mockito.when(mockedOwner.getPet(newPet.getName().toLowerCase(), false)).thenReturn(pet);

		String resultString = petController.processUpdateForm(newPet, result, mockedOwner, model);

		// Assert
		// Result should be the createOrUpdatePetForm view
		assertEquals(VIEWS_PETS_CREATE_OR_UPDATE_FORM, resultString);
		// Verify that there is an error of duplicate name
		assertTrue(result.hasErrors());
		assertEquals("duplicate", result.getFieldError("name").getCode());
		// Verify that the already existing pet is put into model
		assertEquals(newPet, model.get("pet"));
	}

	@Test
	public void test_processUpdateForm_set_pet_birthdate_in_future_return_error() {
		Pet pet = new Pet();
		pet.setId(1);
		pet.setName("Drak mrak");
		pet.setType(petTypes.get("lizard"));
		pet.setBirthDate(LocalDate.now().plusDays(1));

		String resultString = petController.processUpdateForm(pet, result, owner, model);

		// Assert
		// Result should be the createOrUpdatePetForm view
		assertEquals(VIEWS_PETS_CREATE_OR_UPDATE_FORM, resultString);
		// Verify that there is an error of birthdate
		assertTrue(result.hasErrors());
		assertTrue(result.getFieldErrors().stream().anyMatch(e -> e.getField().equals("birthDate")));
		// Verify that the model contains unsaved pet
		assertEquals(pet, model.get("pet"));
	}

	@Test
	public void test_processUpdateForm_set_pet_birthdate_today_successfully() {
		Pet pet = new Pet();
		pet.setId(1);
		pet.setName("Drak mrak");
		pet.setType(petTypes.get("lizard"));
		pet.setBirthDate(LocalDate.now());

		String resultString = petController.processUpdateForm(pet, result, mockedOwner, model);

		// Assert
		// Result should be the OK redirect URL
		assertEquals(processCreationOrUpdateFormOk, resultString);
		// Verify that there are no errors
		assertFalse(result.hasErrors());
		// Verify that the owner's addPet method was called with the pet
		Mockito.verify(mockedOwner).addPet(pet);
		// Verify that the owner's save method was called
		Mockito.verify(ownerRepository).save(mockedOwner);
	}

}
