package org.springframework.samples.petclinic.UnitTests;

import org.junit.jupiter.api.Test;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.Visit;

import static org.junit.jupiter.api.Assertions.*;

public class OwnerTests {

	@Test
	public void test_set_and_get_address_city_telephone() {
		Owner owner = new Owner();
		owner.setAddress("123 Main St");
		owner.setCity("Springfield");
		owner.setTelephone("5551234");

		// Assert that address, city and telephone is as set values
		assertEquals("123 Main St", owner.getAddress());
		assertEquals("Springfield", owner.getCity());
		assertEquals("5551234", owner.getTelephone());
	}

	@Test
	public void test_addPet_valid_pet_to_owner_pets_successfully() {
		Owner owner = new Owner();
		Pet pet = new Pet();

		owner.addPet(pet);

		// Assert that pet is in owner list of pets
		assertFalse(owner.getPets().isEmpty());
		assertTrue(owner.getPets().contains(pet));
	}

	@Test
	public void test_getPet_by_id_successfully() {
		Owner owner = new Owner();
		Pet pet1 = new Pet();
		Pet pet2 = new Pet();
		owner.addPet(pet1);
		owner.addPet(pet2);
		pet1.setId(1);
		pet2.setId(2);
		pet1.setName("Max");
		pet2.setName("Garfield");

		Pet result = owner.getPet(2);

		// Assert that pet with required id is returned
		assertEquals(pet2, result);
	}

	@Test
	public void test_getPet_by_id_pet_isNew_should_return_null() {
		Owner owner = new Owner();
		Pet pet1 = new Pet();
		Pet pet2 = new Pet();
		owner.addPet(pet1);
		owner.addPet(pet2);
		pet1.setName("Max");
		pet2.setName("Garfield");

		Pet result = owner.getPet(2);

		// Assert that null is returned
		assertNull(result);
	}

	@Test
	public void test_getPet_by_id_pet_is_not_new_should_return_valid_pet() {
		Owner owner = new Owner();
		Pet pet1 = new Pet();
		Pet pet2 = new Pet();
		owner.addPet(pet1);
		owner.addPet(pet2);
		pet2.setId(2);
		pet1.setName("Max");
		pet2.setName("Garfield");

		Pet result = owner.getPet(2);

		// Assert that pet with required id is returned
		assertEquals(pet2, result);
	}

	@Test
	public void test_getPet_no_pets_exist_should_return_null() {
		Owner owner = new Owner();

		Pet result = owner.getPet(1);

		// Assert that nothing (null) is returned
		assertNull(result);
	}

	@Test
	public void test_getPet_existingName_ignoreNew_false_successfully() {
		Owner owner = new Owner();
		Pet pet1 = new Pet();
		pet1.setName("Max");
		Pet pet2 = new Pet();
		pet2.setName("Buddy");
		owner.addPet(pet1);
		owner.addPet(pet2);

		Pet result = owner.getPet("Max", false);

		// Assert that pet "Max" is returned
		assertEquals(pet1, result);
	}

	@Test
	public void test_getPet_existing_Name_ignoreNew_true_should_return_null() {
		Owner owner = new Owner();
		Pet pet1 = new Pet();
		pet1.setName("Max");
		Pet pet2 = new Pet();
		pet2.setName("Buddy");
		owner.addPet(pet1);
		owner.addPet(pet2);

		Pet result = owner.getPet("Max", true);

		// Assert that nothing (null) is returned
		assertNull(result);
	}

	@Test
	public void test_getPet_non_existing_name_should_return_null() {
		Owner owner = new Owner();
		Pet pet1 = new Pet();
		pet1.setName("Max");
		Pet pet2 = new Pet();
		pet2.setName("Buddy");
		owner.addPet(pet1);
		owner.addPet(pet2);

		Pet result = owner.getPet("Tom", false);

		// Assert that null is returned
		assertNull(result);
	}

	@Test
	public void test_addVisit_valid_petId_and_visit_successfully() {
		int petId = 1;
		Pet pet = new Pet();
		Visit visit = new Visit();
		Owner owner = new Owner();

		owner.addPet(pet);
		pet.setId(petId);

		owner.addVisit(petId, visit);

		// Assert that pet visits contains the new visit
		assertTrue(pet.getVisits().contains(visit));
	}

	@Test
	public void test_addVisit_with_null_petId_should_throw_error() {
		Owner owner = new Owner();
		Visit visit = new Visit();

		// Assert that IllegalArgumentException is thrown
		Exception e = assertThrows(IllegalArgumentException.class, () -> owner.addVisit(null, visit));
		// Verify that error message is correct
		assertEquals("Pet identifier must not be null!", e.getMessage());
	}

	@Test
	public void test_addVisit_non_existing_petId_should_throw_error() {
		Owner owner = new Owner();
		Visit visit = new Visit();

		// Assert that IllegalArgumentException is thrown
		Exception e = assertThrows(IllegalArgumentException.class, () -> owner.addVisit(1, visit));
		// Verify that error message is correct
		assertEquals("Invalid Pet identifier!", e.getMessage());
	}

	@Test
	public void test_addVisit_with_null_visit_object_and_valid_petId_should_throw_error() {
		Owner owner = new Owner();
		Visit visit = null;

		// Assert that IllegalArgumentException is thrown
		Exception e = assertThrows(IllegalArgumentException.class, () -> owner.addVisit(1, visit));
		// Verify that error message is correct
		assertEquals("Visit must not be null!", e.getMessage());
	}

}
