package org.springframework.samples.petclinic.UnitTests;

import jakarta.validation.ConstraintViolation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.samples.petclinic.model.Person;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import jakarta.validation.Validator;

import java.util.Locale;
import java.util.Set;

public class PersonTests {

	private Validator createValidator() {
		LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
		localValidatorFactoryBean.afterPropertiesSet();
		return localValidatorFactoryBean;
	}

	@Test
	public void test_create_person_with_first_and_last_name() {
		Person person = new Person();
		person.setFirstName("John");
		person.setLastName("Wick");

		Assertions.assertEquals("John", person.getFirstName());
		Assertions.assertEquals("Wick", person.getLastName());
	}

	@Test
	public void test_cannot_create_person_with_empty_first_name() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		Person person = new Person();
		person.setFirstName("");
		person.setLastName("Wick");

		Validator validator = createValidator();
		Set<ConstraintViolation<Person>> constraintViolations = validator.validate(person);

		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Person> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("firstName", violation.getPropertyPath().toString());
		Assertions.assertEquals("must not be blank", violation.getMessage());
	}

	@Test
	public void test_cannot_create_person_with_empty_last_name() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		Person person = new Person();
		person.setFirstName("John");
		person.setLastName("");

		Validator validator = createValidator();
		Set<ConstraintViolation<Person>> constraintViolations = validator.validate(person);

		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Person> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("lastName", violation.getPropertyPath().toString());
		Assertions.assertEquals("must not be blank", violation.getMessage());
	}

}
