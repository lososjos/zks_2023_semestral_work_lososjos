package org.springframework.samples.petclinic.UnitTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.owner.PetTypeFormatter;

import java.text.ParseException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

public class PetTypeFormatterTests {

	private OwnerRepository ownerRepository;

	private ArrayList<PetType> petTypes;

	private Map<String, PetType> petTypesMapped;

	@BeforeEach
	public void setUp() {
		ownerRepository = mock(OwnerRepository.class);

		List<String> types = Arrays.asList("dog", "bird", "cat", "hamster", "lizard", "snake");
		petTypes = new ArrayList<>();
		petTypesMapped = new HashMap<>();
		for (String type : types) {
			PetType petType = new PetType();
			petType.setName(type);
			petTypes.add(petType);
			petTypesMapped.put(type, petType);
		}
	}

	@Test
	public void test_parse_valid_pet_type_name() throws ParseException {
		PetTypeFormatter formatter = new PetTypeFormatter(ownerRepository);
		Mockito.when(ownerRepository.findPetTypes()).thenReturn(petTypes);
		String petTypeText = "dog";

		PetType result = formatter.parse(petTypeText, Locale.getDefault());

		// Assert that result type is expected petType
		assertEquals(petTypesMapped.get(petTypeText), result);
	}

	@Test
	public void test_parse_empty_text() {
		PetTypeFormatter formatter = new PetTypeFormatter(ownerRepository);

		// Assert that Parse exception is thrown
		Exception e = assertThrows(ParseException.class, () -> formatter.parse("", Locale.ENGLISH));
		// Assert that exception message is correct
		assertEquals("type not found: ", e.getMessage());
	}

	@Test
	public void test_parse_non_existent_pet_type_name() {
		PetTypeFormatter formatter = new PetTypeFormatter(ownerRepository);
		String text = "dragon";
		// When findPetTypes is called all pet types are returned
		Mockito.when(ownerRepository.findPetTypes()).thenReturn(petTypes);

		// Assert that parse exception is thrown
		Exception e = assertThrows(ParseException.class, () -> formatter.parse(text, Locale.ENGLISH));
		assertEquals("type not found: " + text, e.getMessage());
	}

}
