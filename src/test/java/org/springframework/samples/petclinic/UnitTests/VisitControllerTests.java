package org.springframework.samples.petclinic.UnitTests;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.samples.petclinic.owner.*;
import org.springframework.validation.BindingResult;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class VisitControllerTests {

	private OwnerRepository ownerRepository;

	private VisitController visitController;

	private Map<String, Object> model;

	private BindingResult result;

	private Validator createValidator() {
		LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
		localValidatorFactoryBean.afterPropertiesSet();
		return localValidatorFactoryBean;
	}

	@BeforeEach
	public void setUp() {
		ownerRepository = mock(OwnerRepository.class);
		visitController = new VisitController(ownerRepository);
		model = new HashMap<>();
		result = new MapBindingResult(new HashMap<>(), "binding");
	}

	@Test
	public void test_loadPetWithVisit_owner_and_pet_successfully_found() {
		int ownerId = 1;
		int petId = 1;

		Owner owner = new Owner();
		Pet pet = new Pet();
		owner.addPet(pet);
		pet.setName("Max");
		pet.setId(petId);
		int oldVisitCount = pet.getVisits().size();

		// When findById is called, owner is returned
		Mockito.when(ownerRepository.findById(ownerId)).thenReturn(owner);

		Visit actualVisit = visitController.loadPetWithVisit(ownerId, petId, model);

		// Assert that crated visit is valid object
		assertNotNull(actualVisit);
		// Assert that one visit was crated for loaded pet
		assertEquals(oldVisitCount + 1, pet.getVisits().size());
		// Assert that correct owner and pet are loaded into model
		assertEquals(owner, model.get("owner"));
		assertEquals(pet, model.get("pet"));
	}

	@Test
	public void test_loadPetWithVisit_owner_not_found_should_throw_error() {
		int ownerId = 1;
		int petId = 1;
		Pet pet = new Pet();
		pet.setId(petId);

		// When findById is called, null is returned
		Mockito.when(ownerRepository.findById(ownerId)).thenReturn(null);

		// Assert that null pointer exception is thrown
		assertThrows(NullPointerException.class, () -> visitController.loadPetWithVisit(ownerId, petId, model));
	}

	@Test
	public void test_loadPetWithVisit_pet_not_found_should_throw_error() {
		int ownerId = 1;
		int petId = 1;
		Owner owner = mock(Owner.class);

		// When findById is called valid owner is returned
		Mockito.when(ownerRepository.findById(ownerId)).thenReturn(owner);
		// When owner.getPet for petId is called null is returned
		Mockito.when(owner.getPet(petId)).thenReturn(null);

		// Assert that null pointer exception is thrown
		assertThrows(NullPointerException.class, () -> visitController.loadPetWithVisit(ownerId, petId, model));
	}

	@Test
	public void test_processNewVisitForm_visit_added_and_saved_successfully() {
		int petId = 1;
		Owner owner = new Owner();
		Pet pet = new Pet();
		owner.addPet(pet);
		pet.setId(petId);
		owner.setId(1);
		Visit visit = new Visit();
		int oldNumberOfVisits = pet.getVisits().size();

		String resultString = visitController.processNewVisitForm(owner, petId, visit, result);

		// Assert that URL is redirect to ownerId page
		assertEquals("redirect:/owners/{ownerId}", resultString);
		// Verify that number of visit was increased
		assertEquals(oldNumberOfVisits + 1, pet.getVisits().size());
		// Verify the new visit is in list of visits
		assertTrue(pet.getVisits().contains(visit));
		// Verify that save the updated owner was called
		Mockito.verify(ownerRepository).save(owner);
	}

	@Test
	public void test_processNewVisitForm_with_errors_and_should_returns_to_form_page() {
		Owner owner = new Owner();
		Pet pet = new Pet();
		owner.addPet(pet);
		pet.setId(1);
		owner.setId(1);
		Visit visit = new Visit();

		Validator validator = createValidator();
		Set<ConstraintViolation<Visit>> constraintViolations = validator.validate(visit);

		ConstraintViolation<Visit> violation = constraintViolations.iterator().next();

		// Add violation to result - description must not be blank violation should be
		// there
		result.rejectValue(violation.getPropertyPath().toString(), violation.getMessage());

		String resultString = visitController.processNewVisitForm(owner, 1, visit, result);

		// Assert that returned URL is still createOrUpdateVisitForm
		assertEquals("pets/createOrUpdateVisitForm", resultString);
	}

	@Test
	public void test_processNewVisitForm_owner_is_null_and_should_throws_NullPointerException() {
		Owner owner = null;
		Visit visit = new Visit();

		// Assert that the NullPointerException is thrown
		assertThrows(NullPointerException.class, () -> visitController.processNewVisitForm(owner, 1, visit, result));
	}

}
