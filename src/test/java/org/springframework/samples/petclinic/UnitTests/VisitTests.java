package org.springframework.samples.petclinic.UnitTests;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.samples.petclinic.owner.Visit;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import java.time.LocalDate;
import java.util.Locale;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VisitTests {

	private Validator createValidator() {
		LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
		localValidatorFactoryBean.afterPropertiesSet();
		return localValidatorFactoryBean;
	}

	@Test
	public void test_create_visit_with_empty_description_should_throw_error() {
		LocaleContextHolder.setLocale(Locale.ENGLISH);
		Visit visit = new Visit();
		String description = "";
		visit.setDescription(description);

		Validator validator = createValidator();
		Set<ConstraintViolation<Visit>> constraintViolations = validator.validate(visit);

		// Assert that description must not be blank
		Assertions.assertEquals(1, constraintViolations.size());
		ConstraintViolation<Visit> violation = constraintViolations.iterator().next();
		Assertions.assertEquals("description", violation.getPropertyPath().toString());
		Assertions.assertEquals("must not be blank", violation.getMessage());

	}

	@Test
	public void test_create_visit_with_default_date_and_valid_description() {
		Visit visit = new Visit();
		LocalDate currentDate = LocalDate.now();
		String description = "Test Description";
		visit.setDescription(description);

		// Assert that default visit date is current date
		assertEquals(currentDate, visit.getDate());
		// Verify that description is correct
		assertEquals(description, visit.getDescription());
	}

	@Test
	public void test_create_visit_sets_date_to_future_date() {
		Visit visit = new Visit();
		LocalDate date = LocalDate.now().plusDays(1);
		visit.setDate(date);
		visit.setDescription("Some description text");

		Validator validator = createValidator();
		Set<ConstraintViolation<Visit>> constraintViolations = validator.validate(visit);

		// Assert that there are no violations
		assertEquals(0, constraintViolations.size());
		// Assert that visit date is in future
		assertEquals(date, visit.getDate());
	}

	@Test
	public void test_create_visit_sets_date_to_past_date() {
		Visit visit = new Visit();
		LocalDate date = LocalDate.now().minusDays(1);
		visit.setDate(date);
		visit.setDescription("Some description text");

		Validator validator = createValidator();
		Set<ConstraintViolation<Visit>> constraintViolations = validator.validate(visit);

		// Assert that there are no violations
		assertEquals(0, constraintViolations.size());
		// Assert that visit date is in past
		assertEquals(date, visit.getDate());
	}

	@Test
	public void test_setDescription_updates_description_field() {
		Visit visit = new Visit();
		String descriptionOld = "Test Description old";
		visit.setDescription(descriptionOld);
		String descriptionUpdated = "Test Description updated";
		visit.setDescription(descriptionUpdated);

		// Verify that visit description is updated
		assertEquals(descriptionUpdated, visit.getDescription());
	}

}
